# uncompyle6 version 3.5.0
# Python bytecode 2.7 (62211)
# Decompiled from: Python 2.7.5 (default, Aug  7 2019, 00:51:29) 
# [GCC 4.8.5 20150623 (Red Hat 4.8.5-39)]
# Embedded file name: ./tim_client.py
# Compiled at: 2019-11-03 02:08:16
import requests, time, os
URL = 'http://10.10.9.14:8080'

def getFile(file_id, url):
    r = requests.get(('{}/?f={}').format(url, file_id))
    file_name = ('{}/{}').format('data', r.text)
    if r.status_code == 404:
        return False
    print ('[DOWNLOAD] {}').format(file_name)
    print ('\tFID={}, status={}').format(file_id, r.status_code)
    print ('\tTIME={}').format(time.ctime(time.time()))
    r = requests.get(('{}/{}').format(url, file_name))
    with open(file_name, 'wb') as (f):
        f.write(r.content)
    return True


if not os.path.isdir('data'):
    os.mkdir('data')
fid = 0
while True:
    if getFile(fid, URL):
        fid += 1
        time.sleep(10)
    else:
        exit(0)
