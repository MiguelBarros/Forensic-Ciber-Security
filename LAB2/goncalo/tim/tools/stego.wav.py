import struct
from sys import argv

"""
Forensics Cyber-Security 2019-2020
@Instituto Superior Tecnico

Simple tool to extract a WAV file from within a container file.
This tool is fully compatible with the material delivered to students.

Based on the official WAV PCM soundfile format.
Read more here http://soundfile.sapp.org/doc/WaveFormat/.
"""

if len(argv) != 2:
    print("[-] Please provide the name of the file you wish to analyse.")
    exit()

filepath = argv[1]
file = open(filepath, "rb")
data = file.read()
data_n_bytes = len(data)

# Store starting byte of the header.
data_header_idx = 0

# Analyse ChunkID (remember there might be a displacement).
while data_header_idx < data_n_bytes - 4:
    chunk_id = struct.unpack("B" * 4, data[data_header_idx:data_header_idx + 4])
    if chunk_id == (82, 73, 70, 70):
        # Found the header.
        break

    data_header_idx += 1

if data_header_idx >= data_n_bytes - 4:
    print("[-] Not a WAV file.")
    exit()

# Keep a byte iterator over the data, but skip header.
data_idx = data_header_idx + 4

# Analyse ChunkSize.
chunk_size = struct.unpack("<I", data[data_idx:data_idx + 4])[0]
data_idx += chunk_size

# The ChunkSize is the size of the entire file minus 8 bytes for the two fields not included: ChunkID and ChunkSize.
data_idx += 8

# Extract payload.
wav_data = data[data_header_idx:data_idx]

# Create output file.
new_file = open(filepath + "-out.wav", "wb")
new_file.write(wav_data)
new_file.close()

print("[+] Successfully extracted a WAV file with %d bytes from a %d bytes file." % (len(wav_data), len(data)))
