#!/bin/bash

if [ -z $1 ]; then
	echo "Usage: generate text_file"
fi

grep -o -E '\w+' $1 | sort -u -f
