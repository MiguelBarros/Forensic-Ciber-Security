#!/bin/bash

# set output dir
OUT_DIR="out"

if [ -z $1 ]; then
	echo "Usage: solve.sh gif_file"
	exit
fi

# create temporary out/ directory for files
if [ ! -d "$OUT_DIR" ]; then
	mkdir $OUT_DIR
fi

cd $OUT_DIR

file_path="$(pwd)/../$1"

# extract metadata information and collect zip file
exiftool -b -comment $file_path > data.zip

# unzip data to get audio file
unzip data.zip
rm data.zip