import struct
from sys import argv

"""
Forensics Cyber-Security 2019-2020
@Instituto Superior Tecnico

Simple tool to extract a PNG file from within a container file.
This tool is fully compatible with the material delivered to students.

Based on the official PNG (Portable Network Graphics) Specification.
Read more here https://tools.ietf.org/html/rfc2083.
"""

if len(argv) != 2:
    print("[-] Please provide the name of the file you wish to analyse.")
    exit()

filepath = argv[1]
file = open(filepath, "rb")
data = file.read()
data_n_bytes = len(data)

# Store starting byte of the header.
data_header_idx = 0

# Check if the first 8 bytes match the signature of a PNG file.
while data_header_idx < data_n_bytes - 8:
    header = struct.unpack("B" * 8, data[data_header_idx:data_header_idx + 8])
    if header == (137, 80, 78, 71, 13, 10, 26, 10):
        # Found the header.
        break

    print("bad luck")
    data_header_idx += 1

if data_header_idx >= data_n_bytes - 8:
    print("[-] Not a PNG file.")
    exit()

# Keep a byte iterator over the data, but skip header.
data_idx = data_header_idx + 8

# Iterate over the bytes of chunks.
while True:
    # Read first chunk, containing the length of the data.
    chunk_data_len = struct.unpack(">I", data[data_idx:data_idx + 4])[0]
    # Skip the data chunk (and the previous length chunk).
    data_idx += chunk_data_len + 4

    # Analyse the type chunk.
    chunk_type = struct.unpack("B" * 4, data[data_idx:data_idx + 4])

    # Ignore the next 8 bytes (4 from the type chunk and four from the CRC chunk).
    data_idx += 8

    if chunk_type == (73, 69, 78, 68):
        # Found the IEND chunk.
        break

# Extract payload.
png_data = data[data_header_idx:data_idx]

# Create output file.
new_file = open(filepath + "-out.png", "wb")
new_file.write(png_data)
new_file.close()

print("[+] Successfully extracted a PNG file with %d bytes from a %d bytes file." % (len(png_data), len(data)))
